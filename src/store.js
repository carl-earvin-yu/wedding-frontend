import Vue from 'vue';
import Vuex from 'vuex';
import config from '@/config'

import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {
    setUpAxios() {
      axios.defaults.baseURL = config.baseURL;
      axios.defaults.timeout = config.timeout;
      axios.defaults.headers.common['api-token'] = '0b2df67a3d934530cc2c7e665ed9c804';
      axios.defaults.headers.common['api-secret-key'] = 'rycAm8B5ltvY'
    },
    init({ dispatch }) {
      return Promise.all([
        dispatch('setUpAxios'),
      ]);
    },
  },
});
