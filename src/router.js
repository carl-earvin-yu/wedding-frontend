import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

import store from './store';

Vue.use(Router);

const storeInit = store.dispatch('init');

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/venue',
      name: 'venue',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "venue" */ './views/Venue.vue'),
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/thanks',
      name: 'thanks',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "thanks" */ './views/Thanks.vue'),
    },
    {
      path: '/rsvp',
      component: () => import(/* webpackChunkName: "rsvp" */ './views/rsvp/RSVP.vue'),
      children: [
        {
          path: '',
          component: () => import(/* webpackChunkName: "rsvp" */ './views/rsvp/RSVP_search.vue'),
          name: 'rsvp_search',
        },
        {
          path: ':code/',
          component: () => import(/* webpackChunkName: "rsvp" */ './views/rsvp/RSVP_detail.vue'),
          name: 'rsvp_detail',
        },
      ],
    },
  ],
  scrollBehavior () {
    return { x: 0, y: 0 };
  },
});

router.beforeEach((to, from, next) => {
  storeInit.then(next)
    .catch(() => {

    });
});

export default router;
