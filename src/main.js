import Vue from 'vue';
import Vuetify from 'vuetify';
import './plugins/vuetify';
import './stylus/main.styl';
import App from './App.vue';
import router from './router';
import store from './store';

import 'leaflet/dist/leaflet.css';


Vue.config.productionTip = false;
Vue.use(Vuetify, {
  theme: {
    primary: '#98dbc6',
    secondary: '#f18d9e',
    accent: '#5bc8ac',
    error: '#992309',
    info: '#e6d72a',
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
